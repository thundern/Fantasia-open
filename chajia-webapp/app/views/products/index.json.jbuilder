json.array!(@products) do |product|
  json.extract! product, :pid, :title, :img, :price
  json.url product_url(product, format: :json)
end
