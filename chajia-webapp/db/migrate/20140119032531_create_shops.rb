class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :surl
      t.boolean :valid

      t.timestamps
    end
  end
end
