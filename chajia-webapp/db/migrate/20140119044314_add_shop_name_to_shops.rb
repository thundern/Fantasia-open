class AddShopNameToShops < ActiveRecord::Migration
  def change
    add_column :shops, :shopname, :string
  end
end
