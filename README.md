[感谢黄亿华老师提供的这么好的垂直爬虫框架  http://www.oschina.net/p/webmagic](http://www.oschina.net/p/webmagic)
>项目分两个部分： 
一个是用java写的基于webmagic爬虫框架开发的定向爬虫，用于爬天猫的数据。
另一个是rails4写的website用于查询天猫商品统计后的数据。以后会增加数据分析模块。

## Features:

* 持久化数据到mysql.
* 图表显示商品历史价格.