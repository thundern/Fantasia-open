class AddIndexToProductsPid < ActiveRecord::Migration
  def change
		add_index :products , :pid , unique: true
  end
end
