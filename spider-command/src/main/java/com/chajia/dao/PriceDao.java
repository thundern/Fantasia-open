package com.chajia.dao;

import com.chajia.model.Price;
import com.chajia.util.DBHelper;
import com.chajia.util.SQLHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-28
 * Time: 上午7:28
 * To change this template use File | Settings | File Templates.
 */
public class PriceDao {
    public boolean insert(Price price) throws Exception{
        String method="insert";
        String sql= SQLHelper.row_insert(Price.TABLE,new String[]{"product_id","price","created_at",
        "updated_at"},null);
        int flag= DBHelper.writeO(method,sql,price.getProduct_id(),price.getPrice(),
                price.getCreated_at(),price.getUpdated_at());
        return flag==1?true:false;
    }
}
