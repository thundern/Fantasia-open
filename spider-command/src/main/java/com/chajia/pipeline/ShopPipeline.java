package com.chajia.pipeline;

import com.chajia.dao.PriceDao;
import com.chajia.dao.ProductDao;
import com.chajia.dao.ShopDao;
import com.chajia.model.Price;
import com.chajia.model.Product;
import com.chajia.model.Shop;
import com.chajia.util.LogUtil;
import com.chajia.util.Transaction;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-1-19
 * Time: 上午11:15
 * 持久化商店数据
 */
public class ShopPipeline implements Pipeline {
    public static Class clazz= ShopPipeline.class;
    ShopDao shopDao=new ShopDao();
    @Override
    public void process(ResultItems resultItems, Task task) {
        //To change body of implemented methods use File | Settings | File Templates.

        String sUrl= resultItems.get("sUrl");
        String shopName=resultItems.get("shopName");
        if (sUrl!=null){
            Shop shop=new Shop(sUrl,shopName);
            try {
                if(!shopDao.isExist(sUrl)){
                    synchronized (shop) {if(!shopDao.insert(shop)){
                        LogUtil.error(clazz, "插入商店失败");
                    }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                Transaction.closeSession(true);
            }
        }
    }
}
